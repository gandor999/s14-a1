console.log("Hello World");

let firstName = "John";
let lastName = "Smith";
let age = 30;

let hobbies = ["Biking", "Mountain Climbing", "Swimming"];
let workAddress = {
	city: "Lincoln",
	houseNumber: "32",
	state: "Nebraska",
	street: "Washington"
}

function fullInfo(firstName, lastName, age, hobbies, workAddress){
	console.log(firstName + " " + lastName + " is " + age + " years of age.");
	console.log("This was printed inside of the function");
	console.log(hobbies);
	console.log("This was printed inside of the function");
	console.log(workAddress);
}

function returnMarriedStatus(){
	return true;
}

let isMarried = returnMarriedStatus();

console.log("First Name: " + firstName);
console.log("Last Name: " + lastName);
console.log("Age: " + age);

console.log("Hobbies:");
console.log(hobbies);

console.log("Work Address:");
console.log(workAddress);

fullInfo(firstName, lastName, age, hobbies, workAddress);

console.log("The value of isMarried is: " + isMarried);




